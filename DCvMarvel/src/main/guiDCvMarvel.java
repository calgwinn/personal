package main;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.JButton;
import ch2.LinkedStringLog;
import javax.swing.JComboBox;
import javax.swing.DefaultComboBoxModel;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class guiDCvMarvel {

    private JFrame frmDcVMarvel;
    private static JTextField hero;
    private static JTextField villain;
    private static JTextField profession;
    private static JComboBox<String> universe;
    private static JComboBox<String> age;
    private static JComboBox<String> gender;
    static int num_surveyed = 0;
    static int dc = 0;
    static int marvel = 0;
    static int np = 0;
    static LinkedStringLog heros = new LinkedStringLog("Heros");
    static LinkedStringLog villains = new LinkedStringLog("Villains");
    static int eighteen_less = 0;
    static int nineteen_twentyfive = 0;
    static int twentysix_thirty = 0;
    static int thirtyone_fifty = 0;
    static int fiftyone_ron = 0;
    static int male = 0;
    static int female = 0;
    static LinkedStringLog professions = new LinkedStringLog("Professions");
    private static String inputString;
    private static int inputInt;

    /**
     * Launch the application.
     */
    public static void main(String[] args) {
        EventQueue.invokeLater(new Runnable() {
            public void run() {
                try {
                    guiDCvMarvel window = new guiDCvMarvel();
                    window.frmDcVMarvel.setVisible(true);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    /**
     * Create the application.
     */
    public guiDCvMarvel() {
        initialize();
    }

    /**
     * Initialize the contents of the frame.
     */
    private void initialize() {
        frmDcVMarvel = new JFrame();
        frmDcVMarvel.setTitle("DC v Marvel Tabulator");
        frmDcVMarvel.setResizable(false);
        frmDcVMarvel.setBounds(100, 100, 299, 245);
        frmDcVMarvel.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frmDcVMarvel.getContentPane().setLayout(null);

        JLabel lblQuestion = new JLabel("Question 1:");
        lblQuestion.setBounds(6, 6, 84, 16);
        frmDcVMarvel.getContentPane().add(lblQuestion);

        JLabel lblQuestion_1 = new JLabel("Hero:");
        lblQuestion_1.setBounds(6, 34, 40, 16);
        frmDcVMarvel.getContentPane().add(lblQuestion_1);

        hero = new JTextField();
        hero.setBounds(58, 29, 130, 26);
        frmDcVMarvel.getContentPane().add(hero);
        hero.setColumns(10);

        JLabel lblVillain = new JLabel("Villain:");
        lblVillain.setBounds(6, 63, 48, 16);
        frmDcVMarvel.getContentPane().add(lblVillain);

        villain = new JTextField();
        villain.setBounds(58, 58, 130, 26);
        frmDcVMarvel.getContentPane().add(villain);
        villain.setColumns(10);

        JLabel lblQuestion_2 = new JLabel("Question 3: ");
        lblQuestion_2.setBounds(6, 90, 77, 16);
        frmDcVMarvel.getContentPane().add(lblQuestion_2);

        JLabel lblQuestion_3 = new JLabel("Question 4: ");
        lblQuestion_3.setBounds(6, 128, 77, 16);
        frmDcVMarvel.getContentPane().add(lblQuestion_3);

        JLabel lblProfession = new JLabel("Profession or Major:");
        lblProfession.setBounds(6, 156, 138, 16);
        frmDcVMarvel.getContentPane().add(lblProfession);

        profession = new JTextField();
        profession.setBounds(156, 151, 130, 26);
        frmDcVMarvel.getContentPane().add(profession);
        profession.setColumns(10);

        JButton btnNext = new JButton("Next");
        btnNext.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e)
            {
                tabulate();
            }
        });
        btnNext.setBounds(16, 184, 117, 29);
        frmDcVMarvel.getContentPane().add(btnNext);

        JButton btnFinish = new JButton("Finish");
        btnFinish.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e)
            {
                tabulate();
                displayResults.results();
            }
        });
        btnFinish.setBounds(166, 184, 117, 29);
        frmDcVMarvel.getContentPane().add(btnFinish);

        universe = new JComboBox<String>();
        universe.setModel(new DefaultComboBoxModel<String>(new String[] {"DC", "Marvel", "Don't care"}));
        universe.setMaximumRowCount(3);
        universe.setToolTipText("");
        universe.setBounds(102, 2, 120, 27);
        frmDcVMarvel.getContentPane().add(universe);

        age = new JComboBox<String>();
        age.setModel(new DefaultComboBoxModel<String>(new String[] {"0-18", "19-25", "26-30", "31-50", "51+"}));
        age.setBounds(95, 86, 96, 27);
        frmDcVMarvel.getContentPane().add(age);

        gender = new JComboBox<String>();
        gender.setModel(new DefaultComboBoxModel<String>(new String[] {"Male", "Female"}));
        gender.setBounds(95, 124, 100, 27);
        frmDcVMarvel.getContentPane().add(gender);
    }

    private void tabulate()
    {
        /*Universe preference*/
        inputInt = universe.getSelectedIndex();
        switch(inputInt)
        {
            case 0: dc++; break;
            case 1: marvel++; break;
            case 2: np++; break;
        }
        universe.setSelectedIndex(0);

        /*Heros*/
        inputString = hero.getText();
        if (heros.contains(inputString)) {}
        else {heros.insert(inputString);}
        hero.setText(null);

        /*Villains*/
        inputString = villain.getText();
        if (villains.contains(inputString)) {}
        else {villains.insert(inputString);}
        villain.setText(null);

        /*Age*/
        int inputAge = age.getSelectedIndex();
        switch (inputAge)
        {
            case 0: eighteen_less++; break;
            case 1: nineteen_twentyfive++; break;
            case 2: twentysix_thirty++; break;
            case 3: thirtyone_fifty++; break;
            case 4: fiftyone_ron++; break;
        }
        age.setSelectedIndex(0);

        /*Gender*/
        inputInt = gender.getSelectedIndex();
        switch(inputInt)
        {
            case 0: male++; break;
            case 1: female++; break;
        }
        gender.setSelectedIndex(0);

        /*Profession*/
        inputString = profession.getText();
        if (professions.contains(inputString)) {}
        else {professions.insert(inputString);}
        profession.setText(null);

        /*Increment number surveyed*/
        num_surveyed++;
    }
}