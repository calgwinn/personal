package main;

import java.io.FileWriter;
import java.io.IOException;
import java.util.Scanner;
import ch2.LinkedStringLog;

public class DCvMarvel
{
    public static void main(String args[]) throws IOException
    {
        /*Scanner*/
        Scanner scanner = new Scanner(System.in);

        /*Number of surveys*/
        int num_surveyed = 0;

        /*Question 1*/
        int dc = 0;
        int marvel = 0;
        int np = 0;

        /*Question 2*/
        LinkedStringLog heros = new LinkedStringLog("Heros");
        LinkedStringLog villains = new LinkedStringLog("Villains");

        /*Question 3*/
        int eighteen_less = 0;
        int nineteen_twentyfive = 0;
        int twentysix_thirty = 0;
        int thirtyone_fifty = 0;
        int fiftyone_ron = 0;

        /*Question 4*/
        int male = 0;
        int female = 0;

        /*Question 5*/
        LinkedStringLog profession = new LinkedStringLog("Professions");

        /*Input number*/
        int input_num;

        /*Input string*/
        String input_string;

        /*Question 1 -- Primer*/
        System.out.println("--Question 1--");
        System.out.println("DC: 1");
        System.out.println("Marvel: 2");
        System.out.println("Don't care: 3");
        input_num = scanner.nextInt();
        if (input_num == 1) {dc++;}
        else if (input_num == 2) {marvel++;}
        else if (input_num == 3) {np++;}
        System.out.println();

        while (input_num == 1 || input_num==2 || input_num==3)
        {
            /*Question 2*/
            System.out.println("--Question 2--");
            System.out.println("Hero:");
            input_string = scanner.next();
            if (heros.contains(input_string)) {}
            else {heros.insert(input_string);}
            System.out.println();

            System.out.println("Villain: ");
            input_string = scanner.next();
            if (villains.contains(input_string)) {}
            else {villains.insert(input_string);}
            System.out.println();

            /*Question 3*/
            System.out.println("--Question 3--");
            System.out.println("18 and less: 1");
            System.out.println("19-25: 2");
            System.out.println("26-30: 3");
            System.out.println("31-50: 4");
            System.out.println("51-Ron: 5");
            input_num = scanner.nextInt();
            switch (input_num)
            {
                case 1: eighteen_less++; break;
                case 2: nineteen_twentyfive++; break;
                case 3: twentysix_thirty++; break;
                case 4: thirtyone_fifty++; break;
                case 5: fiftyone_ron++; break;
            }
            System.out.println();

            /*Question 4*/
            System.out.println("--Question 4--");
            System.out.println("Male: 1");
            System.out.println("Female: 2");
            input_num = scanner.nextInt();
            if (input_num == 1) {male++;}
            else if (input_num == 2) {female++;}
            System.out.println();

            /*Question 5*/
            System.out.println("--Question 5--");
            input_string = scanner.next();
            if (profession.contains(input_string)) {}
            else {profession.insert(input_string);}
            System.out.println();

            /*Increment number surveyed*/
            num_surveyed++;

            /*Question 1*/
            System.out.println("--Question 1--");
            System.out.println("DC: 1");
            System.out.println("Marvel: 2");
            System.out.println("Don't care: 3");
            input_num = scanner.nextInt();
            if (input_num == 1) {dc++;}
            else if (input_num == 2) {marvel++;}
            else if (input_num == 3) {np++;}
            System.out.println();
        }

        /*Close scanner*/
        scanner.close();

        /*Write results to a file*/
        FileWriter writer = new FileWriter("/Users/calebgwinner/Desktop/dcvmarvel.txt");
        writer.write("DC: " + dc + "\n");
        writer.append("Marvel: " + marvel + "\n");
        writer.append("Don't care: " + np + "\n");
        writer.append(heros.toString2() + "\n");
        writer.append(villains.toString2() + "\n");
        writer.append("18-: " + eighteen_less + "\n");
        writer.append("19-25: " + nineteen_twentyfive + "\n");
        writer.append("26-30: " + twentysix_thirty + "\n");
        writer.append("31-50: " + thirtyone_fifty + "\n");
        writer.append("51-Ron: " + fiftyone_ron + "\n");
        writer.append("Male: " + male + "\n");
        writer.append("Female: " + female + "\n");
        writer.append(profession.toString2() + "\n");
        writer.append("Number of surveys: " + num_surveyed + "\n");

        /*Close file*/
        writer.close();
    }
}