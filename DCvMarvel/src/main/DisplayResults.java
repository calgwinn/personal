package main;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class displayResults {

    private JFrame frmResults;

    /**
     * Launch the application.
     */
    public static void results() {
        EventQueue.invokeLater(new Runnable() {
            public void run() {
                try {
                    displayResults window = new displayResults();
                    window.frmResults.setVisible(true);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    /**
     * Create the application.
     */
    public displayResults() {
        initialize();
    }

    /**
     * Initialize the contents of the frame.
     */
    private void initialize() {
        frmResults = new JFrame();
        frmResults.setTitle("Results");
        frmResults.setResizable(false);
        frmResults.setBounds(100, 100, 295, 459);
        frmResults.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frmResults.getContentPane().setLayout(null);

        JLabel lblDc = new JLabel("DC:");
        lblDc.setBounds(6, 6, 61, 16);
        frmResults.getContentPane().add(lblDc);

        JLabel lblMarvel = new JLabel("Marvel:");
        lblMarvel.setBounds(6, 34, 61, 16);
        frmResults.getContentPane().add(lblMarvel);

        JLabel lblDontCare = new JLabel("Don't care:");
        lblDontCare.setBounds(6, 62, 82, 16);
        frmResults.getContentPane().add(lblDontCare);

        JLabel lblHeros = new JLabel("Heros:");
        lblHeros.setBounds(6, 90, 61, 16);
        frmResults.getContentPane().add(lblHeros);

        JLabel lblVillains = new JLabel("Villains:");
        lblVillains.setBounds(6, 118, 61, 16);
        frmResults.getContentPane().add(lblVillains);

        JLabel label = new JLabel("0-18:");
        label.setBounds(6, 146, 61, 16);
        frmResults.getContentPane().add(label);

        JLabel label_1 = new JLabel("19-25:");
        label_1.setBounds(6, 174, 61, 16);
        frmResults.getContentPane().add(label_1);

        JLabel label_2 = new JLabel("26-30:");
        label_2.setBounds(6, 202, 61, 16);
        frmResults.getContentPane().add(label_2);

        JLabel label_3 = new JLabel("31-50:");
        label_3.setBounds(6, 230, 61, 16);
        frmResults.getContentPane().add(label_3);

        JLabel label_4 = new JLabel("51+:");
        label_4.setBounds(6, 258, 61, 16);
        frmResults.getContentPane().add(label_4);

        JLabel lblMale = new JLabel("Male:");
        lblMale.setBounds(6, 286, 61, 16);
        frmResults.getContentPane().add(lblMale);

        JLabel lblFemale = new JLabel("Female:");
        lblFemale.setBounds(6, 314, 61, 16);
        frmResults.getContentPane().add(lblFemale);

        JLabel lblProfession = new JLabel("Profession/Major:");
        lblProfession.setBounds(6, 342, 118, 16);
        frmResults.getContentPane().add(lblProfession);

        JLabel lblNumberSurveyed = new JLabel("Number Surveyed:");
        lblNumberSurveyed.setBounds(6, 370, 118, 16);
        frmResults.getContentPane().add(lblNumberSurveyed);

        JLabel lblNewLabel = new JLabel(String.valueOf(guiDCvMarvel.dc));
        lblNewLabel.setBounds(219, 6, 61, 16);
        frmResults.getContentPane().add(lblNewLabel);

        JLabel lblNewLabel_1 = new JLabel(String.valueOf(guiDCvMarvel.marvel));
        lblNewLabel_1.setBounds(219, 34, 61, 16);
        frmResults.getContentPane().add(lblNewLabel_1);

        JLabel lblNewLabel_2 = new JLabel(String.valueOf(guiDCvMarvel.np));
        lblNewLabel_2.setBounds(219, 62, 61, 16);
        frmResults.getContentPane().add(lblNewLabel_2);

        JLabel lblNewLabel_3 = new JLabel(guiDCvMarvel.heros.toString2());
        lblNewLabel_3.setBounds(219, 90, 61, 16);
        frmResults.getContentPane().add(lblNewLabel_3);

        JLabel lblNewLabel_4 = new JLabel(guiDCvMarvel.villains.toString2());
        lblNewLabel_4.setBounds(219, 118, 61, 16);
        frmResults.getContentPane().add(lblNewLabel_4);

        JLabel lblNewLabel_5 = new JLabel(String.valueOf(guiDCvMarvel.eighteen_less));
        lblNewLabel_5.setBounds(219, 146, 61, 16);
        frmResults.getContentPane().add(lblNewLabel_5);

        JLabel lblNewLabel_6 = new JLabel(String.valueOf(guiDCvMarvel.nineteen_twentyfive));
        lblNewLabel_6.setBounds(219, 174, 61, 16);
        frmResults.getContentPane().add(lblNewLabel_6);

        JLabel lblNewLabel_7 = new JLabel(String.valueOf(guiDCvMarvel.twentysix_thirty));
        lblNewLabel_7.setBounds(219, 202, 61, 16);
        frmResults.getContentPane().add(lblNewLabel_7);

        JLabel lblNewLabel_8 = new JLabel(String.valueOf(guiDCvMarvel.thirtyone_fifty));
        lblNewLabel_8.setBounds(219, 230, 61, 16);
        frmResults.getContentPane().add(lblNewLabel_8);

        JLabel lblNewLabel_9 = new JLabel(String.valueOf(guiDCvMarvel.fiftyone_ron));
        lblNewLabel_9.setBounds(219, 258, 61, 16);
        frmResults.getContentPane().add(lblNewLabel_9);

        JLabel lblNewLabel_10 = new JLabel(String.valueOf(guiDCvMarvel.male));
        lblNewLabel_10.setBounds(219, 286, 61, 16);
        frmResults.getContentPane().add(lblNewLabel_10);

        JLabel lblNewLabel_11 = new JLabel(String.valueOf(guiDCvMarvel.female));
        lblNewLabel_11.setBounds(219, 314, 61, 16);
        frmResults.getContentPane().add(lblNewLabel_11);

        JLabel lblNewLabel_12 = new JLabel(guiDCvMarvel.professions.toString2());
        lblNewLabel_12.setBounds(219, 342, 61, 16);
        frmResults.getContentPane().add(lblNewLabel_12);

        JLabel lblNewLabel_13 = new JLabel(String.valueOf(guiDCvMarvel.num_surveyed));
        lblNewLabel_13.setBounds(219, 370, 61, 16);
        frmResults.getContentPane().add(lblNewLabel_13);

        JButton btnDone = new JButton("Done");
        btnDone.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e)
            {
                System.exit(0);
            }
        });
        btnDone.setBounds(93, 398, 117, 29);
        frmResults.getContentPane().add(btnDone);
    }

}
