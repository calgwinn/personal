/**
 * On page load function
 */
function onLoad() {
    //Get the background
    if(cache.get("background")) {
        console.log("Getting image from cache");
        var background = cache.get("background");
    } else {
        fetch('https://api.nasa.gov/planetary/apod?api_key=GsOsUTYoRxapjo4ja41C9voGuGe7VcRRgY98OU1d', {method: 'GET'}) 
            .then((response) => {return response.json()})
            .then((json) => {
                console.log(json.url)
                var background = json.url;
                cache.set("background", background);
            });
    }
    document.getElementById("background").style.background = "url(" + decodeURI(background) + ") no-repeat center";
    document.getElementById("background").style["backgroundSize"] = "cover";
    //Set time and greeting
    setTime();

    //Get the quote
    if(cache.get("quote")){
        console.log("Getting quote from cache");
        var quote = cache.get("quote");
        var author = cache.get("author");
    } else {
        fetch('https://quotes.rest/qod?language=en', {method: 'GET'}) 
            .then((response) => {return response.json()})
            .then((data) => {
                var quote = data.contents.quotes[0].quote;
                var author = data.contents.quotes[0].author;
                cache.set("quote", quote)
                cache.set("author", author)
            });
    }
    document.getElementById("quote").innerText = quote
    document.getElementById("author").innerText = author

    //Get the weather
    fetch('https://api.openweathermap.org/data/2.5/weather?zip=48033,us&units=imperial&appid=d5e5c3bbe8e178cd055850e60c1872da', {method: 'GET'}) 
        .then((response) => {return response.json()})
        .then((data) => {
            document.getElementById("weather").innerText = Math.floor(data.main.temp) + " °	";
            console.log(data.weather[0].icon);
            var imgUrl = "http://openweathermap.org/img/wn/" + data.weather[0].icon + "@2x.png";
            console.log(imgUrl);
            document.getElementById("weatherImage").src = imgUrl;
        });
}

/**
 * Sets the time and greeting
 */
function setTime() {
    var date = new Date();
    var hour = date.getHours();
    var min = date.getMinutes();
    document.getElementById("time").innerText = (hour > 12 ? hour-12 : hour) + ":" + (min < 10 ? "0" + min : min);
    document.getElementById("ampm").innerText = hour < 12 ? "am" : "pm";

    if(hour < 12) 
        var greeting = "morning";
    else if(hour >= 12 && hour <= 16) 
        var greeting = "afternoon";
    else 
        var greeting = "evening";
    document.getElementById("greeting").innerText = "Good " + greeting;
}

/**
 * Updates the time once per second
 */
setInterval(function() { 
     setTime();
}, 1000);

/**
 * Caching mechanism 
 */
var cache = {
    set: function(key, value) {
  
      var val = {
        value: value,
        expires: new Date().setHours(24, 1, 0, 0)
      };
  
      localStorage.setItem(key, JSON.stringify(val));
    },
  
    get: function(key) {
      if(!key) {
        return null;
      }
  
      var raw = localStorage.getItem(key);
  
      if (!raw) {
        return null;
      }
  
      var json = JSON.parse(raw);
      var now = new Date().getTime();
  
      if (json.expires && now > json.expires) {
        return null;
      }
  
      return json.value;
    }
  };