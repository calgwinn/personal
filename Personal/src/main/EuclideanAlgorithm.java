package main;


public class EuclideanAlgorithm
{
    public static void main(String args[])
    {
        System.out.println(1/2);
        System.out.println(1%2);

        /*The Euclidean Algorithm*/
        int a = 5895;
        int b = 1232;
        int r = b;

        while (b != 0)
        {
            r = a%b;
            //System.out.println("r: " + r);
            a = b;
            //System.out.println("a: " + a);
            b = r;
            //System.out.println("b " + b);
        }

        System.out.println(a);

        int numer = 6, denom = 9;
        System.out.println("Reducing " + numer + "/" + denom);

        a = numer;
        b = denom;

        while (b != 0)
        {
            r = a%b;
            a = b;
            b = r;
        }

        int gcd = a;

        System.out.println("GCD: " + gcd);
        System.out.print(numer + "/" + denom + " reduced is: ");
        numer = numer / gcd;
        denom = denom / gcd;
        System.out.println(numer + "/" + denom);
    }
}
