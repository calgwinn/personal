package main;

//************************************************************
//Quadratic Formula: this program calculates the answer
//to a quadratic formula given b, a, and c.
//
//Author: Caleb Gwinner
//Date: 10.24.14
//
//Filename: QuadraticFormula
//************************************************************

import java.util.Scanner;

public class QuadraticFormula
{
    //Declarations
    static double a; 			//A variable for a
    static double b; 			//A variable for b
    static double c; 			//A variable for c
    static double underSqrt; 	//A variable for the info under the square root
    static double answerPos;	//A variable for the positive result
    static double answerNeg; 	//A variable for the negative result

    //Initializer statement to instantiate the scanner
    static Scanner dataIn = new Scanner(System.in);

    public static void main (String args[])
    {
        //Prompt for and save the values of the variables
        System.out.print("Please enter the value of a: ");
        a = dataIn.nextDouble();
        System.out.print("Please enter the value of b: ");
        b = dataIn.nextDouble();
        System.out.print("Please enter the value of c: ");
        c = dataIn.nextDouble();
        System.out.println("Thank you. Calculating the answer...");
        System.out.println();

        //Calculate the results
        underSqrt = b*b - 4*a*c;
        if (underSqrt < 0)
        {
            System.out.println("Error: imaginary number:");
            System.out.println("negative under the square root");
        }
        else
        {
            answerPos = ((b*-1) + Math.sqrt(underSqrt)) / (2*a);
            answerNeg = ((b*-1) - Math.sqrt(underSqrt)) / (2*a);
            //Output the results
            System.out.println("The positive answer is: " + answerPos);
            System.out.println("The negative answer is: " + answerNeg);
        }

    }
}