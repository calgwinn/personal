package command_line.logic;

import command_line.logic.exceptions.InvalidPhaseException;
import command_line.logic.exceptions.InvalidPointsException;

public class Player {

    /**
     * The name of the player
     * @default Unknown
     */
    private String name = "Unknown";

    /**
     * Player's current phase
     * @default 1
     */
    private int phase = 1;

    /**
     * Player's total points
     * @default 0
     */
    private int points = 0;

    /**
     * Is this player the current dealer?
     * @default false
     */
    private boolean isDealer = false;

    /**
     * Constructor with name and dealer flag
     * @param name New name
     * @param isDealer Is this player the dealer
     */
    @SuppressWarnings("unused")
    public Player(String name, boolean isDealer) {
        this.setName(name);
        this.setDealer(isDealer);
    }

    /**
     * Constructor with name
     * @param name New name
     */
    public Player(String name) {
        this.setName(name);
    }

    /**
     * Argumentless constructor
     */
    @SuppressWarnings("unused")
    public Player() {}

    /**
     * Get this player's name
     * @return name
     */
    public String getName() {
        return name;
    }

    /**
     * Get this player's current phase
     * @return phase
     */
    public int getPhase() {
        return phase;
    }

    /**
     * Get this player's points
     * @return points
     */
    public int getPoints() {
        return points;
    }

    /**
     * Get whether or not this player is the current dealer
     * @return isDealer
     */
    @SuppressWarnings("unused")
    public boolean isDealer() {
        return isDealer;
    }

    /**
     * Set this player's name
     * @param name The new name
     * @return The player's new name
     */
    public String setName(String name) {
        //Make sure the name is less than 10 characters
        if(name.length() > 10) name = name.substring(0, 9);
        this.name = name;
        return this.name;
    }

    /**
     * Increment this player's phase
     * @return The player's new phase
     */
    public int incPhase() {
        if(phase != 10) phase++;
        return phase;
    }

    /**
     * Decrement this player's phase
     * @return The player's new phase
     */
    @SuppressWarnings("unused")
    public int decPhase() {
        if(phase > 1) phase--;
        return phase;
    }

    /**
     * Set the player's current phase
     * @param phase The new phase
     * @return The player's new phase
     * @throws InvalidPhaseException If the given value for phase is invalid
     */
    public int setPhase(int phase) throws InvalidPhaseException {
        if(checkPhase(phase)) this.phase = phase;
        return this.phase;
    }

    /**
     * Add to this player's points
     * @param points Number of points to add to this player's total
     * @return This player's new total points
     * @throws InvalidPointsException If the given value for points is invalid
     */
    public int addPoints(int points) throws InvalidPointsException {
        if(checkPoints(points)) this.points += points;
        return this.points;
    }

    /**
     * Remove points from this player's total
     * @param points Number of points to remove from this player's total
     * @return This player's new total points
     * @throws InvalidPointsException If the given value for points is invalid
     */
    public int remPoints(int points) throws InvalidPointsException {
        if(checkPoints(points)) this.points -= points;
        return this.points;
    }

    /**
     * Set this player's point total
     * @param points The player's new point total
     * @return this player's new total points
     * @throws InvalidPointsException If the given value for points is invalid
     */
    public int setPoints(int points) throws InvalidPointsException {
        if(checkPoints(points)) this.points = points;
        return this.points;
    }

    /**
     * Set whether or not this player is the current dealer
     * @param dealer True/false that this player is the current dealer
     */
    public void setDealer(boolean dealer) {
        this.isDealer = dealer;
    }

    /**
     * Sorts the given list of players by phase and then score
     * @param playerList Array of players to sort
     * @return Sorted list
     */
    public static Player[] sortByRank(Player[] playerList) {
        //Manually implemented bubble sort since we
        //have to sort values inside of an object

        int n = playerList.length;

        //First we'll sort on the phase
        for(int i = 0; i < n-1; i++) {
            for(int j = 0; j < n-i-1; j++) {
                if(playerList[j].getPhase() < playerList[j+1].getPhase()) {
                    Player temp = playerList[j];
                    playerList[j] = playerList[j+1];
                    playerList[j+1] = temp;
                }
            }
        }

        //Now we need to sort by points - we'll modify the bubble sort
        for(int i = 0; i < n-1; i++) {
            for(int j = 0; j < n - i - 1; j++) {
                if((playerList[j].getPhase() == playerList[j + 1].getPhase()) &&
                    (playerList[j].getPoints() > playerList[j + 1].getPoints())) {
                        Player temp = playerList[j];
                        playerList[j] = playerList[j + 1];
                        playerList[j + 1] = temp;
                    }
                }
        }

        return playerList;
    }

    /**
     * Runs sanity checks on incoming point values
     * @param points Point value to check
     * @return true of the points are a valid value
     * @throws InvalidPointsException If the given value for points is invalid
     */
    private static boolean checkPoints(int points) throws InvalidPointsException {
        //Point must be greater than 0
        if(points<0) throw new InvalidPointsException("Points cannot be negative");

        //Points must be in a multiple of 5
        if(points%5 != 0) throw new InvalidPointsException("Points must be a multiple of 5");

        return true;
    }

    /**
     * Run sanity checks on incoming phase values
     * @param phase Phase value to check
     * @return true if the phase is a valid value
     * @throws InvalidPhaseException If the given value for phase is invalid
     */
    private static boolean checkPhase(int phase) throws InvalidPhaseException {
        //Phase must be equal to or greater than one
        if(phase < 1) throw new InvalidPhaseException("Phase must be greater than 1");

        //Phase must be less than or equal to ten
        if(phase > 10) throw new InvalidPhaseException("Phase must be less than 10");

        return true;
    }
}
