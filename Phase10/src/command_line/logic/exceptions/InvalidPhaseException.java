package command_line.logic.exceptions;

/**
 * Exception class for throwing exceptions specific to invalid phase values
 */
public class InvalidPhaseException extends Exception {
    public InvalidPhaseException(String message) {
        super(message);
    }
}
