package command_line.logic.exceptions;

/**
 * Exception class for throwing exceptions specific to invalid point values
 */
public class InvalidPointsException extends Exception {
    public InvalidPointsException(String message) {
        super(message);
    }
}
