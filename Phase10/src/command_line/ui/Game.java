package command_line.ui;

import command_line.logic.Player;
import command_line.logic.exceptions.InvalidPhaseException;
import command_line.logic.exceptions.InvalidPointsException;

import java.util.Scanner;

public class Game {

    private Player[] players;
    private Scanner scanner;
    private final String DIVIDER = "--------------------------------";

    public static void main(String[] args) {
        Game game = new Game();
        game.run();
    }

    private void run() {
        scanner = new Scanner(System.in);
        runGame();
    }

    private void runGame() {
        //Get the number of players to create the players object
        System.out.print("Enter the number of players > ");
        int numPlayers;

        try {
            numPlayers = Integer.parseInt(scanner.nextLine());
        } catch(NumberFormatException e) {
            System.out.println("Please enter a number");
            runGame();
            return;
        }

        if(numPlayers < 1) {
            System.out.println("Who do you think you are?? I don't think so...");
            runGame();
            return;
        } else if(numPlayers == 1) {
            System.out.println("You're really not going to play by yourself are you??");
            runGame();
            return;
        }

        players = new Player[numPlayers];

        //Get the names of the players
        for(int i = 0; i < numPlayers; i++) {
            System.out.print("Enter the name of player " + (i+1) + " > ");
            players[i] = new Player(scanner.nextLine());
        }

        //Loop until they chose to end the game
        int option;
        do {
            System.out.println();
            System.out.println("Main Menu");
            System.out.println(DIVIDER);
            System.out.println("1: Add round");
            System.out.println("2. Show Stats");
            System.out.println("3: Print phases");
            System.out.println("4: Edit Player");
            System.out.println("5: End game");
            System.out.print("Your entry > ");

            try {
                option = Integer.parseInt(scanner.nextLine());
            } catch(NumberFormatException e) {
                System.out.println("Please enter a number");
                option = 10;
            }

            switch(option) {
                case 1: addRound(); break;
                case 2: showStats(true); break;
                case 3: showPhases(); break;
                case 4: editPlayer(); break;
                default: break; //Do nothing
            }
        } while(option != 5);

        //Once they end the game, print out the final stats
        showStats(false);

        //Close the scanner - only here
        //If done elsewhere it will close System.in and nothing will work
        scanner.close();
        System.exit(0);
    }

    private void addRound() {
        for(int i = 0; i < players.length; i++)
            addRoundPlayer(i);

        //Once the round is added automatically print out the stats by rank
        showStats(false);
    }

    private void addRoundPlayer(int playerNum) {
            Player player = players[playerNum];
            System.out.println(player.getName());
            System.out.println(DIVIDER);

            //Get and validate points
            int points = 0;
            boolean validInput = false;
            while(!validInput) {
                System.out.print("Points > ");
                try {
                    points = Integer.parseInt(scanner.nextLine());
                    player.addPoints(points);
                    validInput = true;
                } catch (InvalidPointsException e) {
                    validInput = false;
                    System.out.println(e.getMessage());
                } catch(NumberFormatException e) {
                    System.out.println("Please enter a number");
                    validInput = false;
                }
            }

            //If they had 0 points the completed the phase, so only ask if they had more than 0
            if(points > 0) {
                //Get and validate the phase flag
                System.out.print("Passed Phase " + player.getPhase() + "? (Y/N) > ");
                boolean passPhase = (scanner.nextLine().equalsIgnoreCase("Y"));

                //If you don't pass the phase you have to have at least 50 points left
                if(!passPhase && points < 50) {
                    System.out.println("*** You've indicated this player did not complete the");
                    System.out.println("phase but has fewer than 50 points. This player");
                    System.out.println("should have at least 50 points.");
                    System.out.println("Please try again.");
                    try {player.remPoints(points);}
                    catch (InvalidPointsException e) {/*The points have already been validated so this should never throw*/ }
                    addRoundPlayer(playerNum);
                }

                //25 is an estimate, if you passed the phase you *probably* have less than 25 points.
                //Ask them if the input is correct, if they say no then send them back and regather
                //the points and phase. If they say it's correct then move on.
                if (passPhase && points > 25) {
                    System.out.println("*** You've indicated that this player completed the ");
                    System.out.println("phase but had more than 25 points, is this correct? (Y/N) > ");
                    if (scanner.nextLine().equalsIgnoreCase("N")) {
                        try {player.remPoints(points);}
                        catch (InvalidPointsException e) {/*The points have already been validated so this should never throw*/ }
                        addRoundPlayer(playerNum);
                    }
                }

                //Once we exit the loop we've gotten valid input, update the player
                if(passPhase) player.incPhase();
            } else {
                player.incPhase();
            }
        System.out.println();
    }

    private void showStats(boolean askForChoice) {
        //Prompt for whether or not to sort by rank
        boolean sortByRank = true; //Default to yes
        if(askForChoice) {
            System.out.println();
            System.out.print("Show stats by rank (Y/N)? > ");
            sortByRank = (scanner.nextLine().equalsIgnoreCase("Y"));
        }

        if(sortByRank) {
            String format = "%-5d %-11s %4d %5d";
            System.out.println("Rank  Name      Phase  Score");
            System.out.println(DIVIDER);
            Player[] p = players.clone();
            p = Player.sortByRank(p);
            for(int i = 0; i < p.length; i++) {
                System.out.println(String.format(format, i+1, p[i].getName(), p[i].getPhase(), p[i].getPoints()));
            }
        } else {
            String format = "%-11s %4d %5d";
            System.out.println("Name       Phase  Score");
            System.out.println(DIVIDER);
            for(Player p : players) {
                System.out.println(String.format(format, p.getName(), p.getPhase(), p.getPoints()));
            }
        }
    }

    private void showPhases() {
        System.out.println();
        System.out.println("The Phases are:");
        System.out.println(DIVIDER);
        System.out.println(" 1: 2 sets of 3");
        System.out.println(" 2: 1 set of 3 and 1 run of 4");
        System.out.println(" 3: 1 set of 4 and 1 run of 4");
        System.out.println(" 4: 1 run of 7");
        System.out.println(" 5: 1 run of 8");
        System.out.println(" 6: 1 run of 9");
        System.out.println(" 7: 2 sets of 4");
        System.out.println(" 8: 7 cards of one color");
        System.out.println(" 9: 1 set of 5 and 1 set of 2");
        System.out.println("10: 1 set of 5 and 1 set of 3");
        System.out.println();
    }

    private void editPlayer() {
        int editOption;
        int playerOption;

        System.out.println();
        System.out.println("Select a player to edit:");
        System.out.println(DIVIDER);
        for(int i=0; i < players.length; i++) {
            System.out.println(i+1 + ": " + players[i].getName());
        }
        System.out.print("Player > ");
        try {
            playerOption = Integer.parseInt(scanner.nextLine());

            //Make sure they selected an actual player
            if(playerOption < 1 || playerOption > players.length) {
                System.out.println("Please select a player number");
                editPlayer();
                return;
            }
        } catch(NumberFormatException e) {
            System.out.println("Please enter a player number");
            return;
        }

        playerOption--;

        do {
            //If we made it this far they've successfully select a player
            System.out.println();
            System.out.println("Edit " + players[playerOption].getName() + ":");
            System.out.println(DIVIDER);
            System.out.println("1: Set Dealer");
            System.out.println("2. Edit Name");
            System.out.println("3: Edit Points");
            System.out.println("4: Edit Phase");
            System.out.println("5: Exit to Main Menu");
            System.out.print("Your entry > ");

            try {
                editOption = Integer.parseInt(scanner.nextLine());
            } catch(NumberFormatException e) {
                System.out.println("Please enter a number");
                editOption = 10;
            }

            switch(editOption) {
                case 1: setDealer(playerOption); break;
                case 2: editName(playerOption); break;
                case 3: editPoints(playerOption); break;
                case 4: editPhase(playerOption); break;
                default: break; //Do nothing
            }
        } while(editOption != 5);
    }

    private void setDealer(int playerNum) {
        //Mark all players as not the dealer
        for(Player p : players)
            p.setDealer(false);

        //Update this player to be the dealer
        players[playerNum].setDealer(true);
    }

    private void editName(int playerNum) {
        System.out.print("New name > ");
        players[playerNum].setName(scanner.nextLine());
    }

    private void editPoints(int playerNum) {
        boolean validInput = false;
        while(!validInput) {
            System.out.println("Current points > " + players[playerNum].getPoints());
            System.out.print("Updated points > ");
            try {
                players[playerNum].setPoints(scanner.nextInt());
                validInput = true;
            } catch (InvalidPointsException e) {
                validInput = false;
                System.out.println(e.getMessage());
            }
        }
    }

    private void editPhase(int playerNum) {
        boolean validInput = false;
        while(!validInput) {
            System.out.println("Current phase > " + players[playerNum].getPhase());
            System.out.print("Updated phase > ");
            try {
                players[playerNum].setPhase(scanner.nextInt());
                validInput = true;
            }
            catch (InvalidPhaseException e) {
                validInput = false;
                System.out.println(e.getMessage());
            }
        }
    }
}
