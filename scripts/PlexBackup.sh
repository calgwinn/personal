#!/bin/bash

GLOBAL_START_TIME=$SECONDS

HOST=10.0.0.77
USER=plex
PWRD=`echo YXNpdHM5OnBsZXgK | base64 --decode`
LOCAL_DIR=/Volumes/PlexBackup/Metadata\ Backups
REMOTE_DIR=/var/lib/plexmediaserver/Library/Application\ Support/Plex\ Media\ Server
LOG_FILE=$LOCAL_DIR/backup.log

echo "..............." >> "$LOG_FILE"
echo `date` >> "$LOG_FILE"

rm -r "$LOCAL_DIR/NewBackups"
mkdir "$LOCAL_DIR/NewBackups"
cd "$LOCAL_DIR/NewBackups"

START=$SECONDS
echo "Backing up Preferences..." >> "$LOG_FILE"
wget ftp://$USER:$PWRD@$HOST//"$REMOTE_DIR"/Preferences.xml
echo "Completed in $((SECONDS-$START)) seconds" >> "$LOG_FILE"

START=$SECONDS
echo "Backing up Metadata..." >> "$LOG_FILE"
wget -m ftp://$USER:$PWRD@$HOST//"$REMOTE_DIR"/Metadata
echo "Completed in $(((SECONDS-$START)/60)) minutes" >> "$LOG_FILE"

START=$SECONDS
echo "Backing up Cache..." >> "$LOG_FILE"
wget -m ftp://$USER:$PWRD@$HOST//"$REMOTE_DIR"/Cache
echo "Completed in $(((SECONDS-$START)/60)) minutes" >> "$LOG_FILE"

START=$SECONDS
echo "Backing up Media data..." >> "$LOG_FILE"
wget -m ftp://$USER:$PWRD@$HOST//"$REMOTE_DIR"/Media


echo "Completed in $(((SECONDS-$START)/60)) minutes" >> "$LOG_FILE"

START=$SECONDS
echo "Rearranging files..." >> "$LOG_FILE"

rm "$LOCAL_DIR/Preferences.xml"
mv "$LOCAL_DIR/NewBackups/Preferences.xml" "$LOCAL_DIR"
echo "Moved preferences" >> "$LOG_FILE"

rm -r "$LOCAL_DIR/Metadata"
mv "$LOCAL_DIR/NewBackups/$HOST$REMOTE_DIR/Metadata" "$LOCAL_DIR"
echo "Moved metadata" >> "$LOG_FILE"

rm -r "$LOCAL_DIR/Cache"
mv "$LOCAL_DIR/NewBackups/$HOST$REMOTE_DIR/Cache" "$LOCAL_DIR"
echo "Moved cache" >> "$LOG_FILE"

rm -r "$LOCAL_DIR/Media"
mv "$LOCAL_DIR/NewBackups/$HOST$REMOTE_DIR/Media" "$LOCAL_DIR"
echo "Moved meadia data" >> "$LOG_FILE"

cd ..
rm -r "$LOCAL_DIR/NewBackups"
echo "Completed in $(((SECONDS-$START)/60)) minutes" >> "$LOG_FILE"

echo "Finished backup in $(((SECONDS-$GLOBAL_START_TIME)/60)) minutes" >> "$LOG_FILE"
echo `date` >> "$LOG_FILE"
