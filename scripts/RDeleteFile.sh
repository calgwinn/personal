#!/bin/bash

if [ "$1" == "" ]; then
    exit 1
fi

cd "$1"

for f in *; do 
    if [[ "$f" == "*" ]]; then
        echo "Dir was empty"
        exit 1
    fi

    if [[ "$f" == *"."* ]]; then
        #echo "Found file" $f
        if [[ "$f" == "$2" ]]; then
            echo "***Deleting" $f
            rm $f
        fi
    else 
        echo "Found dir" $f
        bash /Users/calebgwinner/git/personal/scripts/RDeleteFile.sh "$f" "$2"
    fi
done
